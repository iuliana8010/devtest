-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 20, 2015 at 01:48 PM
-- Server version: 5.5.20-log
-- PHP Version: 5.3.10

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `devtest`
--

-- --------------------------------------------------------

--
-- Table structure for table `reply`
--

CREATE TABLE IF NOT EXISTS `reply` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_review` int(11) NOT NULL,
  `text` text NOT NULL,
  `user_name` int(11) NOT NULL,
  `ip_user` varchar(15) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE IF NOT EXISTS `reviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rating` int(11) NOT NULL,
  `Content` text NOT NULL,
  `user_name` text NOT NULL,
  `email` text NOT NULL,
  `ip_user` varchar(15) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=39 ;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`id`, `rating`, `Content`, `user_name`, `email`, `ip_user`, `date`) VALUES
(18, 0, 'comment here', 'ion', '', '', '2015-03-20'),
(19, 0, 'comment here', 'maria', '', '127.0.0.1', '0000-00-00'),
(20, 0, 'comment here or not', 'iuli', 'email', '127.0.0.1', '2015-03-20'),
(21, 0, 'new one', 'iuli', 'iuli@yyy.ro', '127.0.0.1', '2015-03-20'),
(22, 0, 'comment here', 'noi', 'undefined', '127.0.0.1', '2015-03-20'),
(23, 0, 'comment here', '', 'undefined', '127.0.0.1', '2015-03-20'),
(24, 0, 'comment here', 'john', '', '127.0.0.1', '2015-03-20'),
(25, 0, 'comment here', 'ion', 'undefined', '127.0.0.1', '2015-03-20'),
(26, 0, 'comment or not', 'me', 'noe@yyyy.com', '127.0.0.1', '2015-03-20'),
(27, 0, 'comment here', '', '', '127.0.0.1', '2015-03-20'),
(28, 0, 'comment here', '', '', '127.0.0.1', '2015-03-20'),
(29, 0, '', '', '', '127.0.0.1', '2015-03-20'),
(30, 0, 'comment here', 'iuli', 'ana@yahoo.com', '127.0.0.1', '2015-03-20'),
(31, 0, 'comment here', 'iuli', 'ana@ggg.to', '127.0.0.1', '2015-03-20'),
(32, 0, 'sometext', 'iuli', 'ana@mmm.ro', '127.0.0.1', '2015-03-20'),
(33, 0, 'ggggggg', 'iuli', 'adddd@hhh.uo', '127.0.0.1', '2015-03-20'),
(34, 0, '', 'iuli', '', '127.0.0.1', '2015-03-20'),
(35, 0, '', 'iuli', '', '127.0.0.1', '2015-03-20'),
(36, 0, 'sssoooo you work or what', 'mmmmmm', 'trrrr@gmail.no', '127.0.0.1', '2015-03-20'),
(37, 0, 'yyyyyy', 'yoooo', 'yo@yo.com', '127.0.0.1', '2015-03-20'),
(38, 0, 'gffgffg', 'mew', 'undefined', '127.0.0.1', '2015-03-20');

-- --------------------------------------------------------

--
-- Table structure for table `usefulness`
--

CREATE TABLE IF NOT EXISTS `usefulness` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_review` int(11) NOT NULL,
  `pro` int(11) NOT NULL,
  `cons` int(11) NOT NULL,
  `ip_user` varchar(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
