    <head>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/reviewstyle.css"/>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,600,300,700" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/angular.min.js"></script>
<script type="text/javascript" src="app/app.js"></script>
    </head>
<body >

<body>

<h1>Review our product.</h1>
<div ng-app="rwApp" ng-controller="rwCtrl">
<div>
<form  class="add-review"  >
Username: <input type="text" ng-model="username" ><br>
<!--Not validated yet-->
Email: <input type="email" ng-model="email"><br>

<!--Placeholder for editior-->
Review:     <textarea ng-model="review" style="width: 400px; height: 200px"></textarea><br>
<button class="" type="submit" ng-click="addReview(review,username,email)" >Add New Review</button>
</form>
</div>

<div class="rw-list">
<table class="table table-striped table-bordered">
        
        <tbody>
            <tr ng-repeat="data in reviews">
                <td>Username:{{data.USER_NAME}} <br>
				Email:{{data.EMAIL}}<br>				
				Review: {{data.CONTENT}}</td>
                
            </tr>
        </tbody>
        </table>
<!--Comment: {{reviews}}-->
  </div>
  </div>
</body>
</html>
