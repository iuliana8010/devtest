//Angular module for reviews
var app = angular.module('rwApp', []);
app.controller('rwCtrl', function($scope, $http) {
getReviews();
    $scope.review= "";
	$scope.username ="";
	$scope.email ="";
		
	//stars scope goes here
	function getReviews(){  
  $http.post("ajax/getReviews.php").success(function(data){
        $scope.reviews = data;
       });
  };
  $scope.addReview = function (review,username,email) {
    $http.post("ajax/addReviews.php?review="+review+"&username="+username+"&email="+email).success(function(data){
        getReviews();
        $scope.reviewInput = "";
	        });
			
  };
  
});

